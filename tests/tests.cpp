#include <iostream>
#include "../mult.h"
#include "../../gtestv2/googletest/include/gtest/gtest.h"

TEST(MultiplicationTests, OneMultOk) {
    ASSERT_EQ(1, mult(1,1));
}

TEST(MultiplicationTests, ZeroMultOk) {
    ASSERT_EQ(0, mult(1231,0));
}

TEST(MultiplicationTests, GenericMult) {
    ASSERT_EQ(10*20, mult(10,20));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}